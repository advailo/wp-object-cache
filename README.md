## Memcached WP Object Cache

## Requirements
* requires php 5.6+
* php-memcached extension enabled
* memcached service installed 

## Installation

* Copy file object-cache.php under wp-content/object-cache.php
* Add lines to wp-config.php
````
define('MEMCACHE_HOST', '127.0.0.1');
define('MEMCACHE_PORT', 11211);

$memcached_servers = [MEMCACHE_HOST.":".MEMCACHE_PORT];
````